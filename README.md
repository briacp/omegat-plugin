### About Okapi Filters Plugin for OmegaT

The **Okapi Filters Plugin for OmegaT** project provides plugins for the **open-source** translation tool [OmegaT](http://www.omegat.org/ OmegaT).

For more information about the Okapi Filters Plugin for OmegaT, see the
[corresponding page on the main wiki](http://okapiframework.org/wiki/index.php?title=Okapi_Filters_Plugin_for_OmegaT).

Bug report and enhancement requests: https://bitbucket.org/okapiframework/omegat-plugin/issues

### Build status

* `dev` branch: [![pipeline status](https://gitlab.com/okapiframework/omegat-plugin/badges/dev/pipeline.svg)](https://gitlab.com/okapiframework/omegat-plugin/commits/dev)
* `master` branch: [![pipeline status](https://gitlab.com/okapiframework/omegat-plugin/badges/master/pipeline.svg)](https://gitlab.com/okapiframework/omegat-plugin/commits/master)

### Downloads

* The latest stable version of the Okapi Filters Plugin for OmegaT is at https://okapiframework.org/binaries/omegat-plugin/
* The nightly version of the Okapi Filters Plugin for OmegaT is at https://gitlab.com/okapiframework/omegat-plugin/-/jobs/artifacts/dev/browse/deployment/done?job=verification